from bokeh.io import curdoc
from bokeh.layouts import column,row
from bokeh.models import ColumnDataSource, CustomJS, Slider, Select, Button, DataTable, CheckboxGroup
from bokeh.plotting import figure
from bokeh_plotting import DashBoard,Graph,Performance

import os
import sys
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path)

from behavior_python.wheel.wheelSession import *
import numpy as np

w = WheelSession('210424_KC105__no_cam_KC',load_flag=True)

# Create plots and widgets
dash = DashBoard(main_data=w.session)

#performance plot
dash.add_graph('performance',Performance())
dash.graphs['performance'].set_cds(dash.shown_data)
dash.graphs['performance'].plot()


# trial_slider
dash.add_widget('trial_slider',
                Slider(start=100, end=1000, value=100, step=10, title='Trial Count'))

#scope selector
dash.add_widget('scope_selector',
                Select(title='Scope of Data',options=list(dash.main_data['data'].keys()),value='overall',width=150))


# novelity
dash.add_widget('is_novel',
                CheckboxGroup(labels=['Include repeat trials'],active=[0]))

def slider_callback(attr, old, new):
    trial_count = dash.widgets['trial_slider'].value
    dash.filter_data(filters={'trial_limit':trial_count})
    for k,g in dash.graphs.items():
        g.set_cds(dash.shown_data)
dash.widgets['trial_slider'].on_change('value', slider_callback)

def scope_callback(attr,old,new):
    scope = dash.widgets['scope_selector'].value
    is_novel = not dash.widgets['is_novel'].active
    dash.set_data(novel=is_novel,scope=scope)
    for k,g in dash.graphs.items():
        g.set_cds(dash.shown_data)
dash.widgets['scope_selector'].on_change('value',scope_callback)

def novel_callback(attr,old,new):
    is_novel = not dash.widgets['is_novel'].active
    scope = dash.widgets['scope_selector'].value
    dash.set_data(novel=is_novel,scope=scope)
    for k,g in dash.graphs.items():
        g.set_cds(dash.shown_data)
dash.widgets['is_novel'].on_change('active',novel_callback)


# Arrange plots and widgets in layouts
control_row = row([w for w in dash.widgets.values()])
layout_column = [control_row]
layout_column.extend([g.fig for g in dash.graphs.values()])
layout = column(layout_column)

curdoc().add_root(layout)