import bokeh.plotting as bok
from bokeh.models import ColumnDataSource, Whisker, TeeHead, HoverTool
import bokeh.palettes as bokcolor


import numpy as np

class DashBoard:
    def __init__(self,main_data):
        self.main_data = main_data
        self.shown_data = self.main_data.copy()
        self.palette = None
        self.widgets = {}
        self.graphs = {}
        self.callbacks = {}

        self.set_data()
        self.filter_data(filters={'trial_limit':len(self.shown_data)})

        print(self.shown_data.keys(),flush=True)

    def add_graph(self,graph_name,graph2add):
        self.graphs[graph_name] = graph2add

    def add_widget(self,widget_name,widget2add):
        self.widgets[widget_name] = widget2add

    def set_data(self,novel=True,scope='both'):
        # set the scope of data
        if novel:
            self.shown_data = self.main_data['novel_stim_data']
        else:
            self.shown_data = self.main_data['data']

        if scope == 'both':
            pass
        elif scope != 'both' and scope not in self.main_data['data'].keys():
            raise ValueError('Data scope {0} does not exist, try one of {1}'.format(scope,list(self.main_data['data'].keys())))
        else:
            self.shown_data = {scope:self.shown_data[scope]}  

    def filter_data(self,filters):
        if filters is None:
            filters = {}
        else:
            if 'trial_limit' in filters.keys():
                if isinstance(self.shown_data,dict):
                    ret = {k: v[v['trial_no'] <= filters['trial_limit']] for k,v in self.shown_data.items()}
                else:
                    ret = self.shown_data[self.shown_data['trial_no'] <= filters['trial_limit']]
            elif 'response_cutoff' in filters.keys():
                if isinstance(self.shown_data,dict):
                    ret = {self.shown_data[k]: v[v['response_latency'] <= filters['response_cutoff']] for k,v in self.shown_data.items()}
                else:
                    ret = self.shown_data[self.shown_data['response_latency'] <= filters['response_cutoff']]
        self.shown_data = ret

class Graph:
    def __init__(self,*args,**kwargs):
        self.cds = None

    def save(self,plotname):
        pass

    def plot(self,*args,**kwargs):
        pass

    def update_cds(self):
        pass

    def create_cds(self):
        pass

    def pretty_axes(self,fig,fontsize):
        """ Makes simple pretty axes"""
        fig.xaxis.axis_line_width=2
        fig.yaxis.axis_line_width=2

        if fontsize is None:
          fontsize = 22
        fig.axis.axis_label_text_font_size = self.pt_font(fontsize)
        fig.axis.axis_label_text_font_style = 'normal'
        fig.axis.major_label_text_font_size = self.pt_font(fontsize)
        return fig

    def pt_font(self,int_font):
      return '{0}pt'.format(int_font)


class Psychometric(Graph):
    def __init__(self,*args,**kwargs):
        self.fontsize = kwargs.get('fontsize',22)
        self.side = kwargs.get('side','right')


    def plot(self,*args,**kwargs):
        # set up axes
        if fig is None:
            fig = bok.figure(toolbar_location=kwargs.get('toolbarlocation','right'),
                             tools = "pan,box_zoom,reset")

        if self.cds is None:
            raise ValueError('Can''t plot without any ColumnDataSource, create that first!')

        # midlines 
        fig.line([0, 0], [0, 1], line_color='gray', line_dash='dotted', line_width=2,line_alpha=0.7, level='underlay')
        fig.line([-100, 100], [0.5, 0.5], line_color='gray', line_dash='dotted', line_width=2,line_alpha=0.7, level='underlay')

        for i,key in self.cds.keys():
            source = self.cds[key]

            fig.circle(x='contrast_x',y='prob_{0}'.format(side),
                           size=kwargs.get('markersize',15),
                           fill_color=area_colors[key],
                           line_color=kwargs.get('markeredgecolor','#ffffff'),
                           line_width=kwargs.get('markeredgewidth',2),
                           source=source,
                           legend_label='{0}'.format(key),
                           name='points'
                           )

            fig.add_layout(
                Whisker(source=source,base='contrast_x',upper='error_up_{0}'.format(side),lower='error_down_{0}'.format(side),
                        line_color=area_colors[key],
                        line_width=kwargs.get('elinewidth',3),
                        upper_head=TeeHead(line_width=0),
                        lower_head=TeeHead(line_width=0)
                        )
                )
        return fig



    def update_cds(self,update_):
        if self.cds is not None:
            pass
        pass


    def create_cds(self,data):
        """ Creates a column data source(cds)"""
        self.cds = {}

        if not isinstance(data,dict):
            raise TypeError('Data should be in an dict. If it''s a DataFrame put it in a dict.')

        for i,key in enumerate(data.keys()):
            if key=='overall':
                continue

            d = data[key]
            curve = WheelCurve(name=key, rawdata=d)

            source = ColumnDataSource(data = {'contrast_x' : 100 * curve.contrast_x,
                                              'prob_right' : curve.proportion_right,
                                              'prob_left' : curve.proportion_left,
                                              'err_right'  : curve.binomial_right,
                                              'err_left'  : curve.binomial_left,
                                              'error_up_right' : [x+e for x,e in zip(curve.proportion_right,curve.binomial_right)],
                                              'error_down_right' : [x-e for x,e in zip(curve.proportion_right,curve.binomial_right)],
                                              'error_up_left' : [x+e for x,e in zip(curve.proportion_left,curve.binomial_left)],
                                              'error_down_left' : [x-e for x,e in zip(curve.proportion_left,curve.binomial_left)],
                                              'trial_count' : curve.counts,
                                              'correct_pcts' : 100 * curve.correct_pcts})

            self.cds[key] = source


class Performance(Graph):
    def __init__(self,*args,**kwargs):
        self.fig = None
        self.cds = {}
        
    def plot(self,*args,**kwargs):
        fontsize = kwargs.get('fontsize',22)
        # set up axes
        fig = bok.figure(toolbar_location=kwargs.get('toolbarlocation','above'),
                     tools = "pan,box_zoom,reset")

        for key in self.cds.keys():
            fig.line(x='trial_no', y='performance_percent',
                     line_color='#148428',
                     line_width=kwargs.get('linewidth',5),
                     source=self.cds[key],
                     legend_label='Accuracy(%)')

        fig = self.pretty_axes(fig,fontsize)
        fig.xaxis.axis_label = 'Time (min)'
        fig.yaxis.axis_label = 'Accuracy(%)'

        fig.yaxis.bounds = (0,100)
        fig.legend.location = 'top_left'
        fig.legend.label_text_font_size = self.pt_font(fontsize-5)
        
        if not kwargs.get('showlegend', False):
            fig.legend.visible = False

        # add hover tool
        fig.add_tools(HoverTool(names=['points'],
                                tooltips=[('Trial No','@trial_no'),
                                          ('Correct(%)','@performance_percent')],
                                mode='mouse'))

        self.fig = fig

    def set_cds(self,data):
        """ Creates a column data source(cds)"""
        
        if not isinstance(data,dict):
            raise TypeError('Data should be in an dict. If it''s a DataFrame put it in a dict.')

        for i,key in enumerate(data.keys()):
            if key=='overall':
                continue
            d = data[key]

            temp = {'time_in_secs' : d['openstart_absolute'] / 60000,
                    'performance_percent' : d['fraction_correct'] * 100,
                    'trial_no' : d['trial_no'] }

            if len(self.cds) != 0:
                print('Updating data',flush=True)
                self.cds[key].data = temp
            else:
                source = ColumnDataSource(data=temp)
                print('Setting Data',flush=True)
                self.cds[key] = source


