import os
import re
import numpy as np
import pandas as pd
from os.path import join as pjoin
import matplotlib.pyplot as plt
import matplotlib.patheffects as path_effects
import matplotlib.gridspec as gridspec
from cycler import cycler
from collections import defaultdict
from behavior_python.utils import *
from ..wheelUtils import *
from ..wheelAnalysis import WheelAnalysis

DUNDER = re.compile(r'^__[^\d\W]\w*__\Z', re.UNICODE)
is_special = DUNDER.match

mplstyledict = {'pdf.fonttype' : 42,
                'ps.fonttype' : 42,
                'axes.titlesize' : 16,
                'axes.labelsize' : 14,
                'axes.facecolor': 'none',
                'axes.linewidth' : 2,
                'axes.spines.right': False,
                'axes.spines.top': False,
                'axes.titleweight': 'bold',
                'ytick.major.size': 9,
                'xtick.major.size': 9,
                'xtick.labelsize' : 12,
                'ytick.labelsize' : 12,
                'xtick.major.width': 1,
                'ytick.major.width': 1,
                'figure.edgecolor': 'none',
                'figure.facecolor': 'none',
                'figure.frameon': False,
                'font.family': ['sans-serif'],
                'font.size'  : 12,
                'font.sans-serif': ['Helvetica',
                                    'Arial',
                                    'DejaVu Sans',
                                    'Bitstream Vera Sans',
                                    'Computer Modern Sans Serif',
                                    'Lucida Grande',
                                    'Verdana',
                                    'Geneva',
                                    'Lucid',
                                    'Avant Garde',
                                    'sans-serif'],             
                'lines.linewidth' : 1.5,
                'lines.markersize' : 4,
                'image.interpolation': 'none',
                'image.resample': False,
}
plt.matplotlib.style.use(mplstyledict)

stim_cycler = (cycler(color=['tab:orange',
                            'tab:green',
                            'tab:red',
                            'tab:purple',
                            'tab:gray',
                            'tab:pink']))

contrast_cycler = (cycler(color=['orangered',
                                 'darkgoldenrod',
                                 'turquoise',
                                 'limegreen',
                                 'darkorchid']))
iter_styles = iter(stim_cycler)
iter_contrasts = iter(contrast_cycler)
stim_styles = defaultdict(lambda : next(iter_styles))
contrast_styles = defaultdict(lambda : next(iter_contrasts))

class WheelBasePlotter:
    def __init__(self):
        pass

    def save(self,plotkey):
        pass

    def pretty_axes(self,ax):
        """ Makes simple pretty axes"""
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.spines['left'].set_linewidth(2)
        ax.spines['bottom'].set_linewidth(2)
        return ax

    def empty_axes(self,ax):
        """ Erases all axes and related ticks and labels"""
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.spines['bottom'].set_visible(False)
        ax.spines['left'].set_visible(False)
        ax.tick_params(bottom="off", left="off")
        ax.set_yticklabels([])
        ax.set_yticks([])
        ax.set_xticklabels([])
        ax.set_xticks([])
        return ax


# def animalSummaries(animalids,*args,**kwargs):
#     """ """
#     if ~isinstance(animalids,list):
#         raise ValueError('You need to provide a list of animal names')
#     fig = plt.figure(figsize=kwargs.get('figsize',(20,20)))
#     nrows = 3
#     ncols = len(animalids)

#     behave_dict = {a_id:WheelBehavior(a_id,'200101',load_behave=True,load_data=True,criteria=criteria,autostart=True) for a_id in animalids}

#     for col,a_id in enumerate(animalids,1):
#         for rows in range(ncols):
#             ax_idx = row * ncols + col
#             ax = fig.add_subplot(nrows, ncols, ax_idx)

#             if row == 0:
#                 ax.set_title('{0}'.format(a_id),fontsize=fontsize,fontweight='bold')
#                 if col!=1:
#                     naked = True
#                     ax.spines['left'].set_visible(False)
#                     ax.set_yticklabels([])
#                 else:
#                     naked=False
#                 behave_dict[a_id].plot('performance', ax=ax, barebones=naked, savefig=False, notitle=True, *args,**kwargs)

#             if row == 1:
#                 if col!=1:
#                     naked = True
#                     ax.spines['left'].set_visible(False)
#                     ax.set_yticklabels([])
#                 else:
#                     naked=False
#                 behave_dict[a_id].plot('responsetimes', ax=ax, barebones=naked, savefig=False, notitle=True, *args,**kwargs)
#             if row == 2:
#                 if col!=1:
#                     naked = True
#                     ax.spines['left'].set_visible(False)
#                     ax.set_yticklabels([])
#                 else:
#                     naked=False
#                 behave_dict[a_id].plot('trialdistributions', ax=ax, barebones=naked, savefig=False, notitle=True, *args,**kwargs)
#             if row == 3:
#                 if col!=1:
#                     naked = True
#                     ax.spines['left'].set_visible(False)
#                 else:
#                     naked=False
#                 behave_dict[a_id].plot('performance', ax=ax, barebones=naked, savefig=False, notitle=True, *args,**kwargs)

#         # self.fig.suptitle('{0} Trianing Summary'.format(self.animalid),fontsize=fontsize+3,fontweight='bold')
#     plt.tight_layout()
#     return fig
