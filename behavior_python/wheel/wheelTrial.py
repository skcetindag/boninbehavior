from behavior_python.utils import *
from .wheelUtils import *
from behavior_python.core import Trial
from .viz.wheelTrialPlotter import WheelTrialPlotter

WHEEL_CIRCUM = 2 * np.pi* 31.2
WHEEL_TICKS_PER_REV = 1024
class WheelTrial(Trial):
    def __init__(self,trial_slice,column_keys,maindata,metadata,*args,**kwargs):
        super().__init__(trial_slice,column_keys,maindata,metadata,*args,**kwargs)
        
        self.logversion = self.metadata['logversion']
        self.answer_time = self.metadata['answertime']
        self.reward_size = self.metadata['rewardsize']
        self.wheel_deg_per_mm = self.metadata['wheelgain'] 
        self.wheel_deg_per_tick = (self.wheel_deg_per_mm * WHEEL_CIRCUM) / WHEEL_TICKS_PER_REV


        self.analyze_trial_data()
        #init trial plotter
        self.plotter = WheelTrialPlotter(self.trial_data,self.metadata)

    def add_data_interval(self,start,end,stimstart):
        """ Adds data from other types of logged data(opto, vstim, lick etc) in a given time interval
            :param start: Starting time of data interval 
            :param end:   End time of data interval
            :type start:  int,float
            :type end:    int, float
            :return:      dictionary of data intervals inside the given interval
            :rtype:       dict
        """
        interval_dict = {}
        vstim = self.data['vstim']        
        if self.logversion == 'pyvstim':
            vstim_interval = vstim[(vstim['presentTime'] >= start) & (vstim['presentTime'] <= end)].dropna()
            # get contrast
            interval_dict['contrast'] = vstim_interval['iFrame'].iloc[0]
            
            # get stim side
            interval_dict['stim_side'] = np.sign(vstim_interval['iTrial'].iloc[0])

            # get stim movement
            interval_dict['stim_pos'] = np.array(vstim_interval['iTrial'])

            # get sftf
            interval_dict['spatial_freq'] = vstim_interval['blank'].iloc[0]
            if 'contrast' in vstim_interval.keys():
                interval_dict['temporal_freq'] = vstim_interval['contrast'].iloc[0]
            else:
                interval_dict['temporal_freq'] = 0

        elif self.logversion == 'stimpy':
            vstim_interval = vstim[(vstim['presentTime'] >= start/1000) & (vstim['presentTime'] <= end/1000)].dropna()
            # this is an offline fix for a vstim logging issue where time increment messes up vstim logging
            vstim_interval = vstim_interval[:-1]

            # fix for swapped left right logging(for data before 26/01/2021)
            vstim_columns_tofix = list(vstim_interval.columns)
            idx_posl = vstim_columns_tofix.index('posx_l')
            idx_posr = vstim_columns_tofix.index('posx_r')
            if idx_posr < idx_posl:
                vstim_columns_tofix[idx_posr] ,vstim_columns_tofix[idx_posl] = vstim_columns_tofix[idx_posl], vstim_columns_tofix[idx_posr]
                vstim_interval.columns = vstim_columns_tofix

            for col in vstim_interval.columns:
                if col == 'code' or col == 'presentTime':
                    continue
                temp_col = vstim_interval[col]
                # if a column has all the same values, take the first entry of the column as the value
                # sf, tf, contrast, stim_side, correct, opto should run through here
                uniq = np.unique(temp_col)
                uniq = uniq[~np.isnan(uniq)]
                if len(uniq) == 1 and col != 'posx_r' and col != 'posx_l':
                    interval_dict[col] = temp_col.iloc[0]
                # if different values exist in the column, take it as a list
                # stim_pos runs through here 
                else:
                    interval_dict[col] = np.array(temp_col)

            # this is to make things easier later in the analysis, maybe not the best way
            interval_dict['contrast'] = interval_dict['contrast_r'] if interval_dict['correct'] else interval_dict['contrast_l']
            interval_dict['stim_pos'] = interval_dict['posx_r'] if interval_dict['correct'] else interval_dict['posx_l']
            interval_dict['spatial_freq'] = interval_dict['sf_r'] if interval_dict['correct'] else interval_dict['sf_l']
            interval_dict['temporal_freq'] = interval_dict['tf_r'] if interval_dict['correct'] else interval_dict['tf_l']
            # interval_dict['stim_side'] = interval_dict['stim_pos'][0]
            interval_dict['stim_side'] =  25 if interval_dict['correct'] else -25

        # add wheel
        wheel = self.data['position']
        wheel_interval = wheel[(wheel['duinotime'] >= start) & (wheel['duinotime'] <= end)].dropna()
        temp_wheel = np.array(wheel_interval[['duinotime', 'value']])

        # resetting the wheel position so the 0 point is aligned with trialstart and converting encoder ticks into degrees
        if len(temp_wheel):
            reset_idx = find_nearest(temp_wheel[:,0],stimstart)[0]
            temp_wheel[:,1] = np.apply_along_axis(self.reset_wheel_pos,0,temp_wheel[:,1],reset_idx) * self.wheel_deg_per_tick

        interval_dict['wheel'] = temp_wheel
        
        # add lick
        try:
            lick = self.data['lick']
            lick_interval = lick[(lick['duinotime'] >= start) & (lick['duinotime'] <= end)].dropna()
            interval_dict['lick'] = np.array(lick_interval[['duinotime', 'value']])
        except:
            interval_dict['lick'] = np.array([])
        # add reward click
        reward = self.data['reward']
        
        reward_interval = reward[(reward['duinotime'] >= start) & (reward['duinotime'] <= end)].dropna()

        reward_given = [self.reward_size] * len(reward_interval)
        reward_interval['value'] = reward_given

        interval_dict['reward'] = np.array(reward_interval[['duinotime', 'value']])

        #add opto
        # TODO : This can cause issues?
        try:
            opto = self.data['opto']
            opto_interval = opto[(opto['duinotime'] >= start) & (opto['duinotime'] <= end)].dropna()
            if len(opto_interval) != 0:
                interval_dict['opto'] = 1
            else:
                # no opto stim in this trial
                interval_dict['opto'] = 0
        except:
            interval_dict['opto'] = 0
        return interval_dict

    def trial_data_from_logs(self):
        """ Iterates over each state change in a DataFrame slice that belongs to one trial(and corrections for pyvstim)
            :return: A list of dictionaries that have data parsed from stimlog and riglog read data
            :rtype: list
        """
        trial_log_data = {'trial_no': int(self.trial_slice[self.column_keys['trialNo']].iloc[0])}
        trial_rows = []
        firstanswer_flag = True
        # iterrows is faster for small DataFrames
        for _,row in self.trial_slice.iterrows():
            curr_trans = row['transition']
            #trial start
            if curr_trans == 'trialstart':
                trial_log_data['trialdur'] = np.array([row[self.column_keys['elapsed']]])

            # stim start    
            elif curr_trans == 'openloopstart':
                trial_log_data['openloopstart'] = row[self.column_keys['elapsed']]
                trial_log_data['correction'] = row[self.column_keys['trialType']]
                trial_log_data['stimdur'] = np.array([row[self.column_keys['elapsed']]])
                trial_log_data['openstart_absolute'] = row[self.column_keys['elapsed']]
            # move start
            elif curr_trans == 'closedloopstart':
                trial_log_data['closedloopdur'] = np.array([row[self.column_keys['elapsed']]])
                
            # correct
            elif curr_trans == 'correct':
                trial_log_data['response_latency'] = row[self.column_keys['stateElapsed']]
                trial_log_data['answer'] = 1
                trial_log_data['closedloopdur'] = np.append(trial_log_data['closedloopdur'],row[self.column_keys['elapsed']])

            # incorrect
            elif curr_trans == 'incorrect':
                trial_log_data['closedloopdur'] = np.append(trial_log_data['closedloopdur'],row[self.column_keys['elapsed']])
                trial_log_data['response_latency'] = row[self.column_keys['stateElapsed']]
                # stimpy has no seperate state for no answer (maybe should be added?) so it is extracted from stateElapsed time
                if self.logversion == 'stimpy':
                    # longer than given time to answer so a no answer
                    if trial_log_data['response_latency']>=float(self.answer_time)*1000:
                        trial_log_data['answer'] = 0
                    else:
                        trial_log_data['answer'] = -1
                elif self.logversion == 'pyvstim':
                    trial_log_data['answer'] = -1
                    trial_log_data['stimdur'] = np.append(trial_log_data['stimdur'],row[self.column_keys['elapsed']])

            # no answer(only enters in pyvstim log)
            elif curr_trans == 'nonanswer':
                trial_log_data['response_latency'] = row[self.column_keys['stateElapsed']]
                trial_log_data['answer'] = 0
                trial_log_data['closedloopdur'] = np.append(trial_log_data['closedloopdur'],row[self.column_keys['elapsed']])
                if self.logversion == 'pyvstim':
                    trial_log_data['stimdur'] = np.append(trial_log_data['stimdur'],row[self.column_keys['elapsed']])
 
            # stim dissappear
            elif curr_trans == 'stimendcorrect' or curr_trans == 'stimendincorrect':
                trial_log_data['stimdur'] = np.append(trial_log_data['stimdur'],row[self.column_keys['elapsed']])

            # correction or trial end
            elif curr_trans == 'correction' or curr_trans == 'trialend':
                if self.logversion == 'pyvstim':
                    if firstanswer_flag:
                        trial_log_data['trialdur'] = np.append(trial_log_data['trialdur'],row[self.column_keys['elapsed']])
                        trial_log_data['correction'] = 0
                        firstanswer_flag = False
                    else:
                        trial_log_data['trialdur'] = np.array([trial_log_data['openloopstart'],trial_log_data['closedloopdur'][1]])
                        if 'correction' in trial_log_data.keys():
                            trial_log_data['correction'] += 1
                        else:
                            trial_log_data['correction'] = 1

                elif self.logversion == 'stimpy':
                    trial_log_data['trialdur'] = np.append(trial_log_data['trialdur'],row[self.column_keys['elapsed']])
                interval_row = self.add_data_interval(start=trial_log_data['trialdur'][0],
                                                      end=trial_log_data['trialdur'][1],
                                                      stimstart=trial_log_data['openloopstart'])
                trial_log_data = {**trial_log_data, **interval_row}
                
                trial_rows.append(trial_log_data)
        return trial_rows

    def reset_wheel_pos(self,traj,reset_idx=0):
        return traj - traj[reset_idx]

    def calc_primary_metrics(self,row,**kwargs):
        """ Calculates the primary metrics from trial_log data (mostly wheel trajectories) 
            :param row : trial_row to be analyzed 
            :type row  : dict
            :return    : dictionary with analyzed trial parameters
            :rtype     : dict
        """
        analyzed_row = {}
        temp_wheel = row['wheel'][:]
        temp_lick = row['lick'][:]

        early_t = kwargs.get('early_t',500)
        reward_t = kwargs.get('reward_t',2000)

        # Get indexes of different time onsets
        stim_onset_idx = find_nearest(temp_wheel[:,0],row['openloopstart'])[0]
        early_onset_idx = find_nearest(temp_wheel[:,0],row['openloopstart']-early_t)[0]
        closedloop_end_idx = find_nearest(temp_wheel[:,0],row['closedloopdur'][1])[0]

        # Get intervals
        stim_wheel_interval = temp_wheel[stim_onset_idx:closedloop_end_idx,:]
        early_wheel_interval = temp_wheel[early_onset_idx:closedloop_end_idx,:]
        
        # Get delta_x and delta_t
        deltas_stim = np.diff(stim_wheel_interval,axis=0)
        deltas_early = np.diff(early_wheel_interval,axis=0)

        # (7) calculate the avg speed 
        inst_speed = np.abs(np.divide(deltas_stim[:,1],deltas_stim[:,0]))
        analyzed_row['avg_speed'] = float(np.mean(inst_speed))

        # (5) calculate the biggest acceleration change
        #get center points for start and end of analysis(stim onset - 500ms to end of closedloop)
        early_onset_idx = find_nearest(temp_wheel[:,0],row['openloopstart']-500)[0]
        closedloop_end_idx = find_nearest(temp_wheel[:,0],row['closedloopdur'][1])[0]
        point = find_reaction_point(temp_wheel[:,1],early_onset_idx,closedloop_end_idx)
        # reaction time is always relative to openloop start aka stim appearance
        analyzed_row['reaction_t'] = temp_wheel[point[0],0] - row['openloopstart']

        # inst_velocity = np.hstack((early_wheel_interval[1:,0].reshape(-1,1),np.divide(deltas_early[:,1],deltas_early[:,0]).reshape(-1,1)))
        # delta2s_early = np.diff(inst_velocity,axis=0)
        # inst_acc = np.hstack((early_wheel_interval[2:,0].reshape(-1,1),np.abs(np.divide(delta2s_early[:,1],delta2s_early[:,0])).reshape(-1,1)))
        # analyzed_row['acc_reaction_t'] = inst_acc[inst_acc[:,1].argmax(),0]

        # (4) calculate the path surplus
        target_choice_idx = point[0]
        path_wheel_interval = temp_wheel[target_choice_idx:closedloop_end_idx,:]
        if len(path_wheel_interval):
            path_real = np.sum(np.abs(np.diff(path_wheel_interval[:,1],axis=0)),axis=0)
            path_ideal = np.abs(row['stim_side']) - np.abs(path_wheel_interval[0,1])
            analyzed_row['path_surplus'] = float((path_real / path_ideal) - 1)
        else:
            analyzed_row['path_surplus'] = np.nan

        # (6) calculate the average lick time relative to  
        # If negative more anticipatory licks
        if len(temp_lick):
            analyzed_row['first_lick_t'] = temp_lick[0,0]
            lick_t_diffs = np.subtract(temp_lick[stim_onset_idx:,0],row['openloopstart'])
            if len(lick_t_diffs):
                analyzed_row['avg_lick_t_diff'] = float(np.mean(lick_t_diffs))
            else:
                analyzed_row['avg_lick_t_diff'] = np.nan
        else:
            # if no lick data
            analyzed_row['first_lick_t'] = np.nan
            analyzed_row['avg_lick_t_diff'] = np.nan

        return analyzed_row

    def analyze_trial_data(self):
        """ Trial data with analyzed parameters and relevant behavioral indexes"""
        cols_to_reset = ['closedloopdur','openloopstart','stimdur','wheel','lick','trialdur','reward']
        log_data = self.trial_data_from_logs()
        if len(log_data):
            row_analyzed = {}
            for row_log in log_data:
                #analyze here
                if len(row_log['wheel']):
                    row_analyzed = self.calc_primary_metrics(row_log)
                row_final = {**row_log, **row_analyzed}

                # reset the times in each trial so that they have their own time frames locked into stimulus start
                row_final = reset_times(row_final,cols_to_reset,anchor='openloopstart')

            self.trial_data.append(row_final)

    def plot(self,plotkey,ax=None,*args,**kwargs):
        print(plotkey)
        if plotkey == 'picture':
            self.plotter.trialPicture(ax=ax,*args, **kwargs)
        else:
            display('{0} plot key not recognized'.format(plotkey))

