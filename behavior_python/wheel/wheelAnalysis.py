import time
import json
from json import JSONEncoder
import pandas as pd
import scipy.stats as sts
from scipy.optimize import curve_fit
from .wheelUtils import *
from ..fit_funcs import *
from ..utils import display
from ..statistics import *


class WheelAnalysis(object):
    def __init__(self,name, data=None):
        self.name = name

        # get rid of overall
        if data is not None:
            self.set_data(data)

        self.analysis = {}
        

    def set_data(self,data):
        self.data = {k:v for k,v in data.items() if k!='overall'}

    def pre_analysis(self):
        self.analysis['summary'] = {}
        for i, key in enumerate(self.data.keys()):
            data_curr = self.data[key]
            summary_dict = {}
            summary_dict['contrast_list'] = np.unique(data_curr['contrast']).tolist()
            # replace left side contrasts with negative value
            data_curr.loc[:,'sided_contrast'] = data_curr.loc[:,['contrast','stim_side']].apply(lambda row: row[0]*np.sign(row[1]) if row[0]!=0 else row[0],axis=1)
            summary_dict['sided_contrast_list'] = np.unique(data_curr['sided_contrast']).tolist()
            
            # get general counts and create a data table using those
            summary_dict['contrast_count'] = []
            summary_dict['left_count'] = []
            summary_dict['right_count'] = []
            for j,contrast in enumerate(summary_dict['sided_contrast_list']):
                contrast_data = data_curr[data_curr['sided_contrast'] == contrast]
                # total count for contrast
                summary_dict['contrast_count'].append(len(contrast_data))
                # left count
                summary_dict['left_count'].append(len(contrast_data[contrast_data['stim_side'] < 0]))
                # right count
                summary_dict['right_count'].append(len(contrast_data[contrast_data['stim_side'] > 0]))

            self.analysis['summary'][key] = summary_dict

    def run_analysis(self,analysis_type=None,*args,**kwargs):
        """ Loops through the conditions(in this case contrasts) and runs various analysis for individual datasets
            Also, creates a data table for each analysis
        """
        self.pre_analysis()

        if not isinstance(analysis_type,list):
            raise TypeError('The analysis_type argument should be a list, got {0} instead'.format(type(analysis_type)))

        if len(self.data.keys()) <2:
            display('Only one type of stimulus present, skipping statistical comparison analysis')
            return 0
        
        for analyz in analysis_type:

            analyz_arg = kwargs.get('{0}_args'.format(analyz),'')
            analyz += '_{0}'.format(analyz_arg)

            self.analysis[analyz] = {}
            display(' {0} ANALYSIS'.format(analyz.upper()))
            
            self.analysis[analyz]['data_types'] = list(self.data.keys())
            self.analysis[analyz]['data_table'] = {}

            analysis_start = time.time()
            if 'mantel_haenzsel' in analyz:
                display('Prepearing data table')
                for i,typ in enumerate(self.analysis[analyz]['data_types']):

                    for j,contrast in enumerate(self.analysis['summary'][typ]['sided_contrast_list']):
                        contrast_data = self.data[typ][self.data[typ]['sided_contrast'] == contrast]
                        # correct answer count
                        count_correct = len(contrast_data[contrast_data['answer'] == 1])
                        # incorrect Answer count
                        count_incorrect = len(contrast_data[contrast_data['answer'] == -1])
                        # no answer count
                        count_noanswer = len(contrast_data[contrast_data['answer'] == 0])

                        table_row = np.array([count_correct,count_incorrect,count_noanswer]).reshape(1,3)
                        # first pass, initiating the table 
                        if i == 0:
                            self.analysis[analyz]['data_table'][contrast] = table_row
                        else:
                            self.analysis[analyz]['data_table'][contrast] = np.vstack((self.analysis[analyz]['data_table'][contrast],table_row))

                if len(self.analysis[analyz]['data_types']) < 2:
                    display('Only single data type present, skipping in-depth analysis')
                else:
                    display('Running analysis...')
                    st = 'Q' if analyz_arg=='' else analyz_arg
                    self.analysis[analyz]['results'],_ = mantel_haenzsel(data=self.analysis[analyz]['data_table'],stats=st)

            elif 'wilcoxon' in analyz:
                # get correct performances in each contrast for different stimuli groups
                display('Prepearing data tables')
                for i,typ in enumerate(self.analysis[analyz]['data_types']):

                    percent_correct = []
                    for j,contrast in enumerate(self.analysis['summary'][typ]['sided_contrast_list']):
                        contrast_data = self.data[typ][self.data[typ]['contrast'] == contrast]
                        
                        # correct answer percentage
                        percent_correct.append(len(contrast_data[contrast_data['answer']==1]) / len(contrast_data))

                    self.analysis[analyz]['data_table'][typ] = percent_correct

                # make a combination of key pairs in data_table and run wilcoxon on each pair
                stats.wilcoxon(a,b)
            
            display('FINISHED {0} ANALYSIS, t={1}'.format(analyz.upper(),time.time()-analysis_start))

    def curve_fit(self,fit_model='erf_psycho2',**fit_args):

        self.pre_analysis()

        fit_key = 'curve_fit_' + fit_model
        self.analysis[fit_key] = {}
        display('FITTING {0}'.format(fit_model.upper()))
        display('Prepearing data table')
        fit_start = time.time()
        for key in self.data.keys():
            display(key)
            data_curr = self.data[key]
            self.analysis[fit_key][key] = {}
            for answer_check,side in enumerate(['left','nogo','right'],start=-1):
                side_dict = {}
                side_dict['contrast'] = self.analysis['summary'][key]['sided_contrast_list']
                for contrast in self.analysis['summary'][key]['contrast_list']: 
                    contrast_data = data_curr[data_curr['contrast']==contrast]

                    # right side
                    right_contrast_data = contrast_data[contrast_data['stim_side'] > 0]
                    cnt_on_right = len(right_contrast_data)
                    if cnt_on_right > 0:
                        perc_on_right = len(right_contrast_data[right_contrast_data['answer']==answer_check])/cnt_on_right
                        conf_on_right = 1.96 * np.sqrt((perc_on_right * (1 - perc_on_right)) / cnt_on_right)  # 95% binomial
                    else:
                        perc_on_right = 0
                        conf_on_right = 0
                    # left side
                    left_contrast_data = contrast_data[contrast_data['stim_side'] < 0]
                    cnt_on_left = len(left_contrast_data)
                    if cnt_on_left > 0:
                        perc_on_left = len(left_contrast_data[left_contrast_data['answer']==-1*answer_check])/cnt_on_left
                        conf_on_left = 1.96 * np.sqrt((perc_on_left * (1 - perc_on_left)) / cnt_on_left)  # 95% binomial
                    else:
                        perc_on_left = 0
                        conf_on_left = 0

                    if contrast == 0:
                        cnt_on_zero = cnt_on_left + cnt_on_right
                        perc_on_zero = (len(left_contrast_data[left_contrast_data['answer']==-1*answer_check]) + len(right_contrast_data[right_contrast_data['answer']==answer_check])) / cnt_on_zero
                        conf_on_zero = 1.96 * np.sqrt((perc_on_zero * (1 - perc_on_zero)) / cnt_on_zero)  # 95% binomial
                        side_dict['percentage'] = [perc_on_zero]
                        side_dict['confidence'] = [conf_on_zero]
                    else:
                        if side_dict.get('percentage',None) is None:
                            side_dict['percentage'] = []
                            side_dict['confidence'] = []

                        side_dict['percentage'].insert(0,perc_on_left)
                        side_dict['percentage'].append(perc_on_right)

                        side_dict['confidence'].insert(0,conf_on_left)
                        side_dict['confidence'].append(conf_on_right)

                x_axis = np.linspace(np.min(side_dict['contrast']), 
                                             np.max(side_dict['contrast']), 
                                             fit_args.get('resolution',100)).reshape(-1, 1)
                if fit_model == 'naive':
                    try:
                        popt,pcov = naive_fit(side_dict['contrast'],
                                              side_dict['percentage'],**fit_args)

                        result = {'pars' : popt,
                                  'pcov' : pcov}
                        
                        side_dict['fitted_x'] = x_axis
                        side_dict['fitted_y'] = np.asarray(sigmoid(x_axis, *result[pars])).reshape(-1, 1)
                    except:
                        display('Failed to fit to data')
                        result = {'pars' : 0,
                                  'popt' : 0}
                        side_dict['fitted_x'] = side_dict['contrast']
                        side_dict['fitted_y'] = side_dict['percentage']

                else: 
                    data = np.vstack((side_dict['contrast'],
                                      self.analysis['summary'][key]['contrast_count'],
                                      side_dict['percentage']))
                    try:
                        fit_pars, fit_L = mle_fit(data,fit_model,side,nfits=fit_args.get('nfits',10))

                        result = {'pars'       : fit_pars,
                                  'likelihood' : fit_L}

                        side_dict['fitted_x'] = x_axis
                        side_dict['fitted_y'] = np.asarray(erf_psycho2(result['pars'],x_axis)).reshape(-1, 1)
                        
                    except:
                        display('Failed to fit to data')
                        side_dict['fitted_x'] = side_dict['contrast']
                        side_dict['fitted_y'] = side_dict['percentage']

                    
                side_dict['results'] = result
                self.analysis[fit_key][key][side] = side_dict

        display('FINISHED FITTING, t={0}'.format(time.time()-fit_start))
        # updates the analysis dict but returns the current fit
        return self.analysis[fit_key]