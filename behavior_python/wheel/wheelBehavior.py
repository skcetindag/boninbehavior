from .wheelSession import *
from behavior_python.core import Behavior
from .viz.wheelBehaviorPlotter import WheelBehaviorPlotter


class WheelBehavior(Behavior):
    
    def __init__(self, animalid, dateinterval='200106',*args,**kwargs):
        super().__init__(animalid,dateinterval,*args,**kwargs)
        self.compile_data()
        self.behavior_data = self.filter_dates(self.behavior_data)

        self.plotter = WheelBehaviorPlotter(self.behavior_data,self.savepath)
        self.plot = self.plotter.plot

    def compile_data(self):
        
        behavior_data = pd.DataFrame()
        
        if self.load_behave:
            missing_sessions = []
            for i,_ in enumerate(self.session_list,1):
                # iterating from reverse
                sesh = self.session_list[-i]
                #check if behavior data exists in one of the session directories inside the analysis directory
                datapath = pjoin(self.analysisfolder,sesh[0],'trainingData.eel').replace("\\","/")
                if not os.path.exists(datapath):
                    missing_sessions.insert(0,sesh)
                else:
                    display('Found data in {0}, loading'.format(datapath))
                    behavior_data = pd.read_pickle(datapath)
                    last_saved_path = datapath
                    break
        else:
            missing_sessions = self.session_list

        display('Adding {0} missing sessions to last analysis data'.format(len(missing_sessions)))
        for i,sesh in enumerate(missing_sessions):
            # gets the session summary from the analysis directory
            display('Analyzing... {0} / {1}'.format(i+1,len(missing_sessions)))
            display('{0}'.format(sesh))
            self.current_sesh = sesh
            temp_session = WheelSession(sesh[0], load_flag=self.load_data)
            session_row = {}
            if temp_session is not None:
                session_row['session'] = temp_session
                session_row['session_no'] = len(behavior_data) + 1
                session_row['session_date'] = missing_sessions[i][1]
                session_row['session_correct_pct'] = temp_session.session['summaries']['overall']['novel_correct_pct']
                session_row['rig'] = temp_session.session['meta']['rig']
                session_row['level'] = temp_session.session['meta']['level']

                sheet_stats = self.get_googlesheet_data(missing_sessions[i][1])
                session_row = {**session_row,**sheet_stats}
                behavior_data = behavior_data.append(session_row, ignore_index=True)
                
            else:
                display("""
                    NO
                    DATA
                    FOR 
                    SESSION""")
                continue
        
        # Failsafe date sorting for non-analyzed all trials and empty sessions(?)
        behavior_data = behavior_data.sort_values('session_date', ascending=True)
        behavior_data.reset_index(inplace=True,drop=True)
        self.behavior_data = behavior_data

        # save behavior data to the last session analysis folder
        self.savepath = pjoin(self.analysisfolder,self.session_list[-1][0]).replace("\\","/")
        savename = pjoin(self.savepath,'trainingData.eel').replace("\\","/")
        
        self.behavior_data.to_pickle(savename)
        display('Behavior data saved in {0}'.format(savename))
        if self.load_behave and len(missing_sessions):
            display('Deleting the old data in {0}'.format(last_saved_path))
            # this is an extra security step, this should not happen
            if last_saved_path != self.session_list[-1][0]:
                os.remove(last_saved_path)


def main():
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Wheel Behavior Data Parsing Tool')

    parser.add_argument('id',metavar='animalid',
                        type=str,help='Animal ID (e.g. KC020)')
    parser.add_argument('-d','--date',metavar='dateinterval',
                        type=str,help='Analysis start date (e.g. 191124)')
    parser.add_argument('-c','--criteria',metavar='criteria',default=[20,0],
                        type=str, help='Criteria dict for analysis thresholding, delimited list input')
    
    '''
    wheelbehave -d 200501 -c "20, 10" KC028
    '''

    opts = parser.parse_args()
    animalid = opts.id
    dateinterval = opts.date
    tmp = [int(x) for x in opts.criteria.split(',')]
    criteria = dict(answered_trials=tmp[0],
                    answered_correct=tmp[1])

    display('Updating Wheel Behavior for {0}'.format(animalid))
    display('Set criteria: {0}: {1}\n\t\t{2}: {3}'.format(list(criteria.keys())[0],
                                                  list(criteria.values())[0],
                                                  list(criteria.keys())[1],
                                                  list(criteria.values())[1]))
    w = WheelBehavior(animalid=animalid, dateinterval=dateinterval, criteria=criteria)

if __name__ == '__main__':
    main()
