import os
import copy
import json
import time
import pickle
from os.path import join as pjoin
from behavior_python.core import Session
from .wheelTrial import *


class WheelSession(Session):
    """ A class to analyze the wheel trainings and experiments

        :param sessiondir: directory of the session inside the presentation folder(e.g. 200619_KC33_wheel_KC)
        :param autoplot: Flag to plot session related graphs automatically
        :type sessiondir:  str
        :type autplot: boolean
        """
    def __init__(self,sessiondir, autoplot=False, filters = None, *args,**kwargs):
        super().__init__(sessiondir,*args,**kwargs)
        
        start = time.time()
        self.autoplot = autoplot

        #add specific data paths
        self.data_paths['meta'] = pjoin(self.savepath,'sessionMeta.json').replace("\\","/")
        self.data_paths['stat'] = pjoin(self.savepath,'sessionStats.json').replace("\\","/")
        self.data_paths['sesh'] = pjoin(self.savepath,'sessionObject.sesh').replace("\\","/")
        self.running_stats = ['response_latency','path_surplus','avg_speed','reaction_t','avg_lick_t_diff','prob']
        
        if self.isSaved() and self.load_flag:
            #load data converts the columns automatically
            display('Loading from {0}'.format(self.savepath))
            self.load_session()
        else:
            # create session attribute that will hold the data and summaries
            self.session['meta'] = self.prep_metadata() # prepare metadata

            self.read_data()
            self.set_statelog_column_keys()

            self.session['rawdata'] = self.get_session_data()
            
            # TODO:this is not pretty 
            self.session['meta']['water_on_rig'] = round(float(np.sum([a[0][1] for a in self.session['rawdata']['reward'] if len(a)])),3)
            
            self.session['data'] = get_running_stats(self.session['rawdata'],stats=self.running_stats)
            
            self.populate_session()
            self.save_session()
            display('Saving data to {0}'.format(self.savepath))  


        # init_plotter
        if kwargs.get('plotwith','default') == 'bokeh':
            from .viz.bokeh.wheelSessionPlotter_bokeh import WheelSessionPlotter
        else:
            from .viz.wheelSessionPlotter import WheelSessionPlotter

        self.plotter = WheelSessionPlotter(self.session,self.savepath)
        self.plot = self.plotter.plot

        end = time.time()
        display('Done! t={0:.2f} s'.format(end-start))
        if self.autoplot:
            self.plot('sessionSummary')

    def prep_metadata(self):
        """ Prepares the metadata of the session"""
        baredate = self.sessiondir.split('/')[-1].split('_')[0]
        date = dt.strftime(dt.strptime(baredate, '%y%m%d').date(), '%d %b %y')
        runname = os.path.splitext(self.data_paths['prefs'])[0]

        tmp = os.path.splitext(self.data_paths['prot'])[0]
        self.level = tmp.split('_')[-1]

        if 'opto' in runname:
            opto = True
        else:
            opto = False

        experimentname = self.sessiondir.split('/')[-1]
        animalid = self.sessiondir.split('_')[1]

        if self.logversion == 'pyvstim':
            rewardsize = eval(self.opts.get('rewardSize','[45,60,5]'))
        elif self.logversion == 'stimpy':
            rewardsize = float(self.opts.get('rewardSize',3))

        return {'logversion'     : self.logversion,
                'baredate'       : baredate,
                'date'           : date,
                'experimentname' : experimentname,
                'animalid'       : animalid,
                'level'          : self.level,
                'contrastset'    : self.opts['contrastVector'],
                'wheelgain'      : float(self.opts.get('wheelGain',3)),
                'answertime'     : float(self.opts.get('closedStimDuration',60)),
                'rewardsize'     : rewardsize,
                'rig'            : self.prefs['tmpFolder'].split('\\')[-1],
                'opto'           : opto}

    def save_session(self):
        """ Saves the session data, meta and stats"""
        save_df = self.session['rawdata'].copy(deep=True)

        # the columns that have numpy.ndarrays need to be saved as lists in DataFrame columns!
        # A copy of the session_data is created with converted columns to be saved
        cols_to_convert = ['closedloopdur','stimdur','trialdur','stim_pos','wheel','lick','reward']
        for col in cols_to_convert:
            save_df[col] = save_df[col].apply(lambda x: x.tolist())

        self.save_session_data(save_df)
        display("Saved session data")

        self.save_dict_json(self.data_paths['meta'], self.session['meta'])
        display("Saved metadata")

        self.save_dict_json(self.data_paths['stat'], self.session['summaries'])
        display("Saved session stats")

        with open(self.data_paths['sesh'],'wb') as fp:
            pickle.dump(self.session,fp)
        display("Saved Session")

    def load_session(self):
        """ Loads the saved session data """
        cols_to_eval = ['closedloopdur','stimdur','trialdur','stim_pos','wheel','lick','reward']
        self.session['rawdata'] = self.load_session_data()

        # necessary columns are turned into numpy.ndarrays
        for col in cols_to_eval:
            self.session['rawdata'][col] = self.session['rawdata'][col].apply(eval).apply(lambda x: np.array(x))
        display('Loaded session data')

        with open(self.data_paths['sesh'],'rb') as fp:
            self.session = pickle.load(fp)
        display('Loaded Session')

    def translate_transition(self,oldState,newState):
        """ A function to be called that add the meaning of state transition into the state DataFrame"""
        curr_key = '{0}->{1}'.format(int(oldState), int(newState))
        state_keys = {'0->1' : 'trialstart',
                      '1->2' : 'openloopstart',
                      '2->3' : 'closedloopstart',
                      '3->4' : 'correct',
                      '3->5' : 'incorrect',
                      '6->0' : 'trialend',
                      '4->6' : 'stimendcorrect',
                      '5->6' : 'stimendincorrect'}

        if self.logversion == 'pyvstim':
            state_keys['3->7'] = 'nonanswer'
            state_keys['2->5'] = 'earlyanswer'
            state_keys['7->1'] = 'correction'
            state_keys['5->7'] = 'na'
            state_keys['7->6'] = 'na'

            if self.level == 'level0':
                state_keys['2->8'] = 'closedloopstart'
                state_keys['8->4'] = 'correct'
        
        return state_keys[curr_key]

    def get_session_data(self):
        """ The main loop where the parsed session data is created
            :return: Parsed sessiondata
            :rtype:  DataFrame
        """
        session_data = pd.DataFrame()
        self.states = self.data['stateMachine']
        self.states['transition'] = self.states.apply(lambda x: self.translate_transition(x[self.column_keys['oldState']],x[self.column_keys['newState']]),axis=1)
        display('Setting global indexing keys for {0} logging'.format(self.logversion))
        
        if self.states.shape[0] == 0:
            display("""NO STATE MACHINE TO ANALYZE
                    LOGGING PROBLEMATIC.
                    SOLVE THIS ISSUE FAST""")
            return None

        trials = np.unique(self.states[self.column_keys['trialNo']])
        # this is a failsafe for some early stimpy data where trial count has not been incremented
        if len(trials) == 1 and len(self.states) > 6 and self.logversion=='stimpy':
            self.extract_trial_count()
            trials = np.unique(self.states[self.column_keys['trialNo']])
        for t in trials:
            trial_slice = self.states[self.states[self.column_keys['trialNo']] == t]
            temp_trial = WheelTrial(trial_slice,
                                    self.column_keys,
                                    logversion=self.logversion,
                                    maindata=self.data,
                                    metadata=self.session['meta'])

            trial_rows = temp_trial.trial_data

            if len(trial_rows):
                session_data = session_data.append(trial_rows, ignore_index=True)
                self.session_trials.append(temp_trial)
            else:
                print(t)
                print(trial_slice)
                print(trial_rows)
                print('OH NO!!!')

        if session_data.empty:
            print('''WARNING THIS SESSION HAS NO DATA
            Possible causes:
            - Session has only one trial with no correct answer''')
            return None
        else:
            return session_data

    def get_summary(self,data_in):
        """ Summarizes the percentages, latency and stim side profiles
            :param data_in: input data, mostly the DataFrame for a specific stimuli(e.g. highSF_lowTF_opto)
            :type data_in: DataFrame
        """
        all_correct_pct = 100 * len(data_in[data_in['answer']==1])/len(data_in)

        novel_data = data_in[data_in['correction']==0]

        #left
        left_data = novel_data[novel_data['stim_side'] < 0]
        left_profile = [-len(left_data[left_data['answer']==1]), -len(left_data[left_data['answer']==-1]),-len(left_data[left_data['answer']==0])]
        #right 
        right_data = novel_data[novel_data['stim_side'] > 0]
        right_profile = [len(right_data[right_data['answer']==1]), len(right_data[right_data['answer']==-1]),len(right_data[right_data['answer']==0])]

        answered_data = novel_data[novel_data['answer']!=0]
        
        novel_correct_pct = 100 * len(novel_data[novel_data['answer']==1])/len(novel_data)
        answered_correct_pct = 100 * len(answered_data[answered_data['answer']==1])/len(answered_data)

        total_latency = np.sum(answered_data['response_latency'])

        return {'all_trials'           : len(data_in),
                'novel_trials'         : len(novel_data),
                'answered_trials'      : len(answered_data),
                'all_correct_pct'      : round(all_correct_pct,3),
                'novel_correct_pct'    : round(novel_correct_pct,3),
                'answered_correct_pct' : round(answered_correct_pct,3),
                'left_profile'         : left_profile,
                'right_profile'        : right_profile,
                'latency'              : round(total_latency / len(answered_data),3),
                'EP'                   : round(calc_EP(data_in),3)}

    def populate_session(self):
        """ Filters the session by dividing into chunks depending on different stim poperties(sf and tf) and opto/nonopto
            It also produces the novel_data used for the analysis"""
        start = time.time()

        self.session['summaries'] = {}
        self.session['novel_stim_data']  = {}
        self.session['data'] = {}

        # summary of the overall data, without seperating stim types and opto
        self.session['summaries']['overall'] = self.get_summary(self.session['rawdata'])
        self.session['novel_stim_data']['overall'] = get_running_stats(self.session['rawdata'][self.session['rawdata']['correction']==0],stats=self.running_stats)
        self.session['data']['overall'] = get_running_stats(self.session['rawdata'],stats=self.running_stats)
        
        sfreq,s_idx = np.unique(self.session['rawdata']['spatial_freq'],return_index=True)
        sfreq = sfreq[s_idx.argsort()]
        tfreq,t_idx = np.unique(self.session['rawdata']['temporal_freq'],return_index=True)
        tfreq = tfreq[t_idx.argsort()]
        print(sfreq)
        print(tfreq)

        if self.session['meta']['opto']:
            opto_loop = [0,1]
            pattern_ids = np.unique(self.session['rawdata']['opto_pattern'])
            # remove nans
            pattern_ids = pattern_ids[~np.isnan(pattern_ids)]
            # remove -1(no opto no pattern, thi is extra failsafe)
            pattern_ids = pattern_ids[pattern_ids >= 0]
        else:
            opto_loop = [0]
            pattern_ids = [-1]        

        # analysing each stim type and opto and opto_pattern seperately
        for opto in opto_loop:
            for i,_ in enumerate(sfreq):
                if sfreq[i] == 0.4 and tfreq[i] == 0.5:
                    key = 'highSF_lowTF'  # PM stim
                elif sfreq[i] == 0.4 and tfreq[i] == 16:
                    key = 'highSF_highTF' #
                elif sfreq[i] == 0.05 and tfreq[i] == 0.5:
                    key = 'lowSF_lowTF'   #
                elif sfreq[i] == 0.05 and tfreq[i] == 16:
                    key = 'lowSF_highTF'  # AL stim
                else:
                    key = 'grating'

                for opto_pattern in pattern_ids:
                    if opto == 0:
                        opto_pattern = -1
                        key_new = key
                    else:
                        key_new = '{0}_opto_{1}'.format(key,int(opto_pattern))

                    display('Looking into {0}'.format(key_new))
                    
                    # stimuli data
                    try:
                        stimuli_data = self.session['rawdata'][(self.session['rawdata']['spatial_freq'] == sfreq[i]) & # spatial freq filter
                                                    (self.session['rawdata']['temporal_freq'] == tfreq[i]) &           # temporal freq filter
                                                    (self.session['rawdata']['opto'] == opto) &                        # opto filter
                                                    (self.session['rawdata']['opto_pattern'] == opto_pattern)]         # opto pattern filter          
                    except:
                        stimuli_data = self.session['rawdata'][(self.session['rawdata']['spatial_freq'] == sfreq[i]) & # spatial freq filter
                                                    (self.session['rawdata']['temporal_freq'] == tfreq[i]) &           # temporal freq filter
                                                    (self.session['rawdata']['opto'] == opto)]                       # opto filter
                    # summaries
                    self.session['summaries'][key_new] = self.get_summary(stimuli_data)

                    stimuli_data_novel = stimuli_data[stimuli_data['correction'] == 0]

                    # data frame data f trials
                    self.session['novel_stim_data'][key_new] = get_running_stats(stimuli_data_novel,stats=self.running_stats)
                    self.session['data'][key_new] = get_running_stats(stimuli_data,stats=self.running_stats)

        end = time.time()
        display("Done t={0} seconds".format(end-start))
    
    def extract_trial_count(self):
        """ Extracts the trial no from state changes, this works for stimpy for now"""
        display('Trial increment faulty, extracting from state changes...')
        self.states.reset_index(drop=True, inplace=True)
        trial_end_list = self.states.index[self.states['oldState']==6].tolist()
        temp_start = 0
        for i,end in enumerate(trial_end_list,1):
            self.states.loc[temp_start:end+1,'cycle'] = i
            temp_start = end + 1

def main():
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Wheel Session Analysis')

    parser.add_argument('expname',metavar='expname',
                        type=str,help='Experiment filename (e.g. 200325_KC020_wheel_KC)')
    parser.add_argument('-l','--load',metavar='load_flag',default=True,
                        type=str,help='Flag for loading existing data')

    opts = parser.parse_args()
    expname = opts.expname
    load_flag = opts.load

    w = WheelSession(sessiondir=expname, load_flag=load_flag)

if __name__ == '__main__':
    main()

