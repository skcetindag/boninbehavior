# Abstract classes for Trial, Session and Behavior
import re
import time
import json
import natsort
import pandas as pd
from os.path import join as pjoin
from .utils import *
from .gsheet_functions import GSheet


DUNDER = re.compile(r'^__[^\d\W]\w*__\Z', re.UNICODE)  # Special method names.

# TODO: NEEDS REWORK?
class Trial:
    def __init__(self,trial_slice,column_keys,maindata,metadata,*args,**kwargs):
        self.trial_slice = trial_slice
        self.column_keys = column_keys
        self.data = maindata
        self.metadata = metadata
        self.trial_data = []

    @classmethod
    def get_trial_data(cls):
        is_special = DUNDER.match  # Local var to speed access.
        items = {}

        for name in dir(cls):
            if not is_special(name):
                value = getattr(cls, name)
                if callable(value):
                    pass
                elif name == 'trial_slice':
                    pass
                elif name == 'column_keys':
                    pass
                elif name == 'data':
                    pass
                elif name == 'plotter':
                    pass
                else:
                    items[name] = value
        return items

    def add_data_interval(self,start,end):
        """ Adds data from other types of logged data(opto, vstim, lick etc) in a given time interval
            :param data  : data dictionary that will be parsed in the givn interval
            :param start : Starting time of data interval 
            :param end   : End time of data interval
            :type data   : dict
            :type start  : int,float
            :type end    : int, float
        """
        interval_dict = {}
        for key in self.data.keys():
            if key == 'vstim':
                vstim = self.data['vstim']        
                vstim_interval = vstim[(vstim['presentTime'] >= start/1000) & (vstim['presentTime'] <= end/1000)].dropna()

                for col in vstim_interval.columns:
                    if col == 'code' or col == 'presentTime':
                        continue
                    temp_col = vstim_interval[col]
                    # if a column has all the same values, take the first entry of the column as the value
                    # sf, tf, contrast, stim_side should run through here
                    uniq = np.unique(temp_col)
                    uniq = uniq[~np.isnan(uniq)]
                    if len(uniq) == 1:
                        interval_dict[col] = temp_col.iloc[0]
                    # if different values exist in the column, take it as a list
                    # stim_pos runs through here 
                    else:
                        interval_dict[col] = np.array(temp_col)
            else:
                temp = self.data[key]
                temp_interval = temp[(temp['duinotime'] >= start) & (temp['duinotime'] <= end)].dropna()
                interval_dict[key] = np.array(temp_interval[['duinotime', 'value']])

        # add opto
        # TODO: COMEBACK TO THIS
        if 'opto' in self.data.keys():
            opto = self.data['opto']
            opto_interval = opto[(opto['duinotime'] >= start) & (opto['duinotime'] <= end)].dropna()
            if len(opto_interval) != 0:
                # no opto stim in this trial
                interval_dict['isOpto'] = 1
            else:
                interval_dict['isOpto'] = 0
        else:
            interval_dict['isOpto'] = 0
        
        return interval_dict


class Session:
    """ A base Session object to be used in analyzing training/experiment sessions
        :param sessiondir: directory of the session inside the presentation folder(e.g. 200619_KC33_wheel_KC)
        :param load_flag:  flag to either load previously parsed data or to parse it again
        :param spit_mat:   flag to make the parser also output a .mat file to be used in MATLAB  scripts
        :type sessiondir:  str
        :type load_flag:   bool
        :type spit_mat:    bool
    """
    def __init__(self, sessiondir, load_flag=False, spit_mat=False, *args, **kwargs):
        self.sessiondir = sessiondir
        # an empty dictionary that can be populated with session related data
        self.session = {}
        self.session_trials = []
        self.load_flag = load_flag
        self.spit_mat = spit_mat

        self.init_data_paths()
        self.logversion = self.get_log_version()
        self.opts, self.params = parseProtocolFile(self.data_paths['prot'])
        self.prefs = parsePref(self.data_paths['prefs'])


    def init_data_paths(self):
        """ Initializes the relevant data paths (log,pref,prot) and creates the savepath"""
        main_data_dir = 'J:/data'

        self.sessionpath = pjoin('J:/data/presentation',self.sessiondir).replace("\\","/")
        self.savepath = pjoin('J:/data/analysis',self.sessiondir).replace("\\","/")
        if os.path.exists(self.sessionpath):
            if not os.path.exists(self.savepath):
                # make dirs
                os.makedirs(self.savepath)
                display('Save path does not exist, created save folder at {0}'.format(self.savepath))

            self.data_paths = {'data': pjoin(self.savepath,'sessionData.csv').replace("\\","/")}
            try:
                for s_file in os.listdir(self.sessionpath):
                    extension = os.path.splitext(s_file)[1]
                    if extension=='.prot':
                        self.data_paths['prot'] = pjoin(self.sessionpath,s_file).replace("\\","/")
                    elif extension == '.prefs':
                        self.data_paths['prefs'] = pjoin(self.sessionpath,s_file).replace("\\","/")
                    elif extension == '.riglog':
                        self.data_paths['riglog'] = pjoin(self.sessionpath,s_file).replace("\\","/")
                    elif extension=='.stimlog':
                        self.data_paths['stimlog'] = pjoin(self.sessionpath,s_file).replace("\\","/")
                    elif extension=='.log':
                        self.data_paths['log'] = pjoin(self.sessionpath,s_file).replace("\\","/")

                # try 1P dir


            # try 2p dir

            # try opto_pattern dir

            except FileNotFoundError:
                if s_file.endswith('.riglog') or s_file.endswith('.stimlog') or s_file.endswith('.log'):
                    display('{0} does not exist, trying .log file from old pyvstim code...'.format(s_file))
                    if s_file.endswith('.log'):
                        if '.log' not in self.data_paths.keys():
                            self.data_paths['stimlog'] = pjoin(self.sessionpath,s_file).replace("\\","/")
                        else:
                            display('.log already found and added to relevant paths.')
                    else:
                        display('{0} is missing. THIS IS BAD, SOMETHING WENT WRONG DURING EXPERIMENT!!'.format(s_file))
                        raise FileNotFoundError
                else:
                    # .prot or .pref missing
                    display('{0} is missing, that should not happen...')
                    # TODO: add a copy from user directory feature in here in the future
        else:
            raise FileNotFoundError('Session directory {0} does not exist!'.format(self.sessiondir))

    def read_data(self):
        """ Reads the data from concatanated riglog and stimlog files"""
        if self.logversion == 'pyvstim':
            self.data, self.comments = parseVStimLog(self.data_paths['log'])
        elif self.logversion == 'stimpy':
            # because there are two different log files now, they have to be combined into a single data variable
            stim_data, stim_comments = parseStimpyLog(self.data_paths['stimlog'])
            rig_data, rig_comments = parseStimpyLog(self.data_paths['riglog'])
            self.data = {**stim_data, **rig_data}
            self.comments = stim_comments + rig_comments
            self.data = extrapolate_time(self.data)

    def get_log_version(self):
        """ Gets the session logging version from existing log file extension."""
        # TODO: Look into this. Version should be printed in stimpy data maybe???
        # self.logversion = self.comments[0].replace('Version','').replace('#','').strip(' ')
        # display('Parsing log version {0}'.format(self.logversion))
        logversion = None
        if 'log' in self.data_paths.keys():
            logversion = 'pyvstim'
        elif 'stimlog' in self.data_paths.keys() and 'riglog' in self.data_paths.keys():
            logversion = 'stimpy'
        return logversion

    def set_statelog_column_keys(self):
        """ Setting log column keys, this is hardcoded for pyvstim but just reads the column keys for stimpy"""
        if self.logversion == 'pyvstim':
            self.column_keys = {'code':'code',
                                'elapsed':'presentTime',
                                'trialNo':'iStim',
                                'newState':'iTrial',
                                'oldState':'iFrame',
                                'trialType':'contrast',
                                'stateElapsed':'blank'}
        elif self.logversion == 'stimpy':
            self.column_keys = {}
            for c in self.data['stateMachine'].columns:
                # only changes the cyce column with trialNo for ease of understanding
                if c == 'cycle':
                    self.column_keys['trialNo'] = c
                else:
                    self.column_keys[c] = c

    def isSaved(self):
        """ Initializes the necessary save paths and checks if data already exists

            :return: Returns a boolean that indicates if there was a saved fike or not
            :rtype:  bool
        """
        self.isLoadable = False
        # the 'stim_pos and 'wheel' columns are saved as lists in DataFrame columns!
        if os.path.exists(self.savepath):
            if os.path.exists(self.data_paths['data']):
                self.isLoadable = True
                display('Found saved data: {0}'.format(self.data_paths['data']))
            else:
                display('{0} exists but no data file is present...'.format(self.savepath))
        else:
            display('THIS SHOULD NOT HAPPEN')

        return self.isLoadable

    def save_session_data(self,data_to_save=None):
        """ Saves the session data as .csv (and .mat file if desired) 
            :param data_to_save : DataFrame of session data to be saved
            :type data_to_save  : pandas.DataFrame
        """
        if data_to_save is None:
            data_to_save = self.session_data
        data_to_save.to_csv(self.data_paths['data'],index=False)
        display("Saved session data")
        if self.spit_mat:
            self.save_as_mat()
            display('Saving .mat file')

    def save_dict_json(self,path,dict_in):
        """ Saves a dictionary as a .json file
            :param path    : path to save the .json file
            :param dict_in : dictionary to be saved
            :type path     : path string
            :type dict_in  : dict
        """
        with open(path,'w') as fp:
            jsonstr = json.dumps(dict_in,indent=4, sort_keys=True)
            fp.write(jsonstr)
        
    def load_session_data(self):
        """Loads the data from J:/analysis/<exp_folder> as a pandas data frame
            :return : Loaded data
            :rtype  : DataFrame
        """
        data = pd.read_csv(self.data_paths['data'])
        return data 

    def load_json_dict(self,path):
        """ Loads .json file as a dict 
            :param path : path of the .json file
            :type path  : path string
        """
        with open(path) as f_in:
            return json.load(f_in)

    def save_as_mat(self):
        """Helper method to convert the data into a .mat file"""
        import scipy.io as sio
        datafile = pjoin(self.savepath,'sessionData.mat').replace("\\","/")
        save_dict = {name: col.values for name, col in self.session_data.items()}
        sio.savemat(datafile, save_dict)


class Behavior:
    """ Analyzes the training progression of animals through multiple sessions
            animalid:     id of the animal to be analyzed(e.g. KC033)
            dateinterval: interval of dates to analyze the data, 
                          can be a list of two dates, or a string of starting date
                          (e.g. ['200110','200619'] OR '200110')
            criteria:     criteria to include/exclude sessions. It's a dictionary with preset keys
                          (e.g. {answered_trials:0, answered_correct:0}
            load_behave:  flag to either load previously analyzed behavior data or to analyze it from scratch
            load_data:    flag to either load previously parsed data or to parse it again
            autostart:    flag to autostart
            autoplot:     flag to automatically plot available plots
            """
    def __init__(self,animalid,dateinterval='210106',criteria=None,*args,**kwargs):

        self.animalid = animalid
        self.presentationfolder = 'J:/data/presentation'
        self.analysisfolder = 'J:/data/analysis'
        self.dateinterval = dateinterval
        self.criteria = criteria
        self.load_behave = kwargs.get('load_behave', True)
        self.load_data = kwargs.get('load_data', True)

        # no point in reading saved behavior data if reanalyzing session data
        if not self.load_data:
            self.load_behave = False

        versions = kwargs.get('versions',['stimpy'])
        display('Analyzing versions: {0}'.format(' '.join(versions)))
        self.get_sessions(versions)

        logsheet = GSheet('Mouse Database_new')
        # below, 2 is the log2021 sheet ID
        self.glog_df = logsheet.read_sheet(2)


    def get_sessions(self,versions):
        """ self.session_list   n x 2 matrix that has the sesion directory names in the 1st column
                                and session dates in datetime format in the 2nd column"""
        search_string_pyvstim = '{0}_wheel'.format(self.animalid)
        search_string_stimpy = '{0}__no_cam'.format(self.animalid)
        opto_search_string_stimpy = '{0}_opto__no_cam'.format(self.animalid)
        session_list = []
        for s in os.listdir(self.presentationfolder):
            if search_string_stimpy in s or opto_search_string_stimpy in s:
                session_list.append(s)
            if 'pyvstim' in versions:
                if search_string_pyvstim in s:
                    session_list.append(s)

        session_list = natsort.natsorted(session_list, reverse=False)

        date_list = [dt.strptime(x.split('_')[0], '%y%m%d') for x in session_list]

        self.session_list = [[session_list[i], date] for i, date in enumerate(date_list)]

    def get_googlesheet_data(self,date):
        sheet_stats = {}

        self.glog_df.dropna(inplace=True)
        #current date data
        row = self.glog_df[(self.glog_df['Date [YYMMDD]']==date) & (self.glog_df['Mouse ID']==self.animalid)]
        if len(row):
            sheet_stats['weight'] = row['weight [g]'].values[0]
            sheet_stats['extra_water'] = row['supp water [µl]'].values[0] 
            sheet_stats['user'] = row['user'].values[0]
            sheet_stats['time'] = row['time [hh:mm]'].values[0]
            
        else:
            display('No Training Log entry {0} {1}'.format(self.animalid, date))
            sheet_stats = {}

        return sheet_stats

    def filter_dates(self,df):
        """Filters the behavior data to analyze only the data in the date range"""

        # dateinterval is a list of two date strings e.g. ['200127','200131']
        if type(self.dateinterval) is not list:
            self.dateinterval = [self.dateinterval]
            # add current day as end date
            self.dateinterval.append(dt.today().strftime('%y%m%d'))

        if len(self.dateinterval) == 2:
            startdate = dt.strptime(self.dateinterval[0], '%y%m%d')
            enddate = dt.strptime(self.dateinterval[1], '%y%m%d')
        else: 
            raise Exception('Date interval wrong!')

        display('Retreiving between {0} - {1}'.format(self.session_list[0][1],self.session_list[-1][1]))

        return df[(df['session_date'] >= startdate) & (df['session_date'] <= enddate)]

    def implement_criteria(self, rowtoappend):
        """ Filters data according to given criteria """
            # for now only thresholds bigger values
        #TODO: FIX ISSUE WITH KEY MISSING!
        if self.criteria is None:
            display('No criteria given, including all {0} sessions'.format(len(self.session_list)))
            return 1
        else:
            satisfied = []
            for k in self.criteria.keys():
                if k not in rowtoappend.keys():
                    display('Criteria {0} not found in data, skipping'.format(k))
                    continue
                else:
                    if rowtoappend[k] >= self.criteria[k]:
                        satisfied.append(1)
                    else:
                        display('{0} not satisfied: {1} < {2}'.format(k, rowtoappend[k], self.criteria[k]))
                        satisfied.append(0)
            if 0 in satisfied:
                return 0
            else:
                return 1
